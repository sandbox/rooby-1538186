<?php

/**
 * Administration page functions for the Message: Editable text module.
 */

/**
 * Messaging form that is used to send arbitrary messages.
 */
function message_et_message_types_list() {
  $header = array(
    t('Message type'),
    t('Operations'),
  );

  $rows = array();

  $message_types = message_et_get_editable_fields();

  foreach ($message_types as $type => $fields) {
    $rows[] = array($type, l(t('Edit'), 'admin/config/system/message-et/' . $type));
  }

  // If there are no types, explain this to the user.
  if (!$rows) {
    $rows[] = array(array('data' => t('There are no message types that allow editable text.'), 'colspan' => 2));
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Edit form for a message type.
 *
 * @param $message_type
 *   The message type entity object.
 */
function message_et_message_type_edit($form, &$form_state, $message_type) {
  $form = array(
    'fields' => array(),
  );

  // Add the message type entity fields.
  field_attach_form('message_type', $message_type, $form['fields'], $form_state);

  // Load the editable fields.
  $fields = message_et_get_editable_fields($message_type->name);

  // Strip anything not editable.
  $form['fields'] = array_intersect_key($form['fields'], $fields);

  $form['fields'] += array(
    '#type' => 'fieldset',
    '#title' => t('Text fields'),
    '#description' => t('The following message fields are editable. Within these fields you may use the following tokens: @tokens', array('@tokens' => implode(', ', $message_type->argument_keys))),
  );

  // Add the message type so that we can use it in submission.
  $form_state['message_type'] = $message_type;

  // Add the editable fields.
  $form['message_et'] = array(
    '#type' => 'value',
    '#value' => serialize($fields),
  );

  // Add the buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/**
 * Validation function for the message type text edit form.
 */
function message_et_message_type_edit_validate($form, &$form_state) {
  field_attach_form_validate('message_type', $form_state['message_type'], $form['fields'], $form_state);
}

/**
 * Submit function for the message type text edit form.
 */
function message_et_message_type_edit_submit($form, &$form_state) {
  $message_type = $form_state['message_type'];
  $fields = unserialize($form_state['values']['message_et']);
  foreach ($fields as $field_name) {
    if (isset($form_state['values'][$field_name])) {
      $message_type->$field_name = $form_state['values'][$field_name];
    }
  }

  // Save and go back.
  $message_type->save();
  $form_state['redirect'] = 'admin/config/system/message-et';
  drupal_set_message(t('Message type text was updated successfully.'));
}
